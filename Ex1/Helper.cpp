#include "Helper.h"
#include "type.h"
#include "Parser.h"
#include <regex>

bool Helper::isInteger(const std::string& s)
{
	int start = (s[0] == '-') ? 1 : 0;
	for (int i = start; i < s.size(); i++)
	{
		if (!isDigit(s[i]))
		{
			return false;
		}
	}

	return true;
}

bool Helper::isBoolean(const std::string& s)
{
	return (s == "True" || s == "False");
}


bool Helper::isString(const std::string& s)
{
	size_t end = s.size() - 1;

	if (s[0] == '\"' && end == s.find('\"', 1))
	{
		return true;
	}
	if (s[0] == '\'' && end == s.find('\'', 1))
	{
		return true;
	}

	return false;

}

bool Helper::isList(const std::string & str, std::vector<Type*>& vec)
{
	size_t end = str.size() - 1;
	
	//Checks if it's a list
	if (str.find('[') != 0 || str.find(']') != end)
		return false;

	std::string list = str.substr(1, end - 1);
	end = list.size();

	int index = 0;

	while (index <= end) {
		std::string variable;
		while (index <= end && list[index] != ',') {
			variable += list[index];
			index++;
		}
		Helper::trim(variable);
		Type* type = Parser::parseString(variable);
		if (!variable.empty() && type != nullptr) {
			vec.push_back(type);
		}
		else {
			vec.clear();
			return false;
		}
		index++;
	}

	return true;
}

bool Helper::isDigit(char c)
{
	return (c >= '0' && c <= '9');
}

void Helper::trim(std::string &str)
{
	rtrim(str);
	ltrim(str);

}

void Helper::rtrim(std::string &str)
{
	size_t endpos = str.find_last_not_of(" \t");
	if (std::string::npos != endpos)
	{
		str = str.substr(0, endpos + 1);
	}
}

void Helper::ltrim(std::string &str)
{
	size_t startpos = str.find_first_not_of(" \t");
	if (std::string::npos != startpos)
	{
		str = str.substr(startpos);
	}
}

void Helper::removeLeadingZeros(std::string &str)
{
	size_t startpos = str.find_first_not_of("0");
	if (std::string::npos != startpos)
	{
		str = str.substr(startpos);
	}
}

std::string Helper::matches(const std::string & str, const std::string & regex)
{
	std::string variable;
	std::smatch match;

	if (std::regex_match(str, match, std::regex(regex))) {
		variable = match[1];
	}

	return variable;
}

bool Helper::isLowerLetter(char c)
{
	return (c >= 'a' && c <= 'z');
}

bool Helper::isUpperLetter(char c)
{
	return (c >= 'A' && c <= 'Z');
}

bool Helper::isLetter(char c)
{
	return (isLowerLetter(c) || isUpperLetter(c));
}

bool Helper::isUnderscore(char c)
{
	return ('_' == c);
}

