#include "parser.h"
#include "IndentationException.h"
#include "SyntaxException.h"
#include "NameErrorException.h"
#include <iostream>

#include "Integer.h"
#include "Boolean.h"
#include "String.h"
#include "Void.h"
#include "List.h"

UnorderedMap Parser::_variables;

Type* Parser::parseString(std::string str) throw()
{
	//Checks if the line starts with a space or a tab
	if (str.find(" ") == 0 || str.find("	") == 0)
		throw IndentationException();

	//Removing spaces from the end of the string
	Helper::rtrim(str);

	if (runCommand(str)) {
		return nullptr;
	}

	//Checking if user entered a variable
	Type* type = getVariableValue(str);
	if (type != nullptr) {
		return type;
	}

	if (isLegalVarName(str) && getType(str) == nullptr)
		throw NameErrorException(str);

	//Checking if user entered a string
	type = getType(str);
	if (type != nullptr)
		return type;

	//Checking if user assign a value to a variable
	if (makeAssignment(str)) {
		Void* v = new Void();
		v->setTemp(true);
		return v;
	}

	throw SyntaxException();
}

Type * Parser::getType(std::string & str)
{
	//Checks if boolean
	if (Helper::isBoolean(str)) {
		bool value = str == "True" ? true : false;
		Boolean* boolean = new Boolean(value);
		return boolean;
	}

	//Checks if integer
	if (Helper::isInteger(str)) {
		try {
			int value = std::stoi(str.c_str());
			Integer* integer = new Integer(value);
			return integer;
		}
		catch (...) {
			throw SyntaxException();
		}
	}

	//Checks if string
	if (Helper::isString(str)) {
		size_t end = str.size() - 1;
		//Checks if the string contains a ' inside it.
		if (str.find('\'', 1) < end) {
			std::string value = str.substr(1, end - 1);
			String* string = new String("\"" + value + "\"");
			return string;
		}
		else {
			std::string value = str.substr(1, end - 1);
			String* string = new String("'" + value + "'");
			return string;
		}
	}

	std::vector<Type*> values;
	if (Helper::isList(str, values)) {
		List* list = new List(values);
		return list;
	}

	return nullptr;
}

bool Parser::isLegalVarName(const std::string & str)
{
	bool legal = true;

	for (int i = 0; i < str.size() && legal; i++) {
		if (!Helper::isDigit(str[i]) && !Helper::isUnderscore(str[i]) && !Helper::isLetter(str[i]))
			legal = false;
	}

	return legal && !Helper::isDigit(str[0]);
}

bool Parser::makeAssignment(const std::string & str)
{
	size_t end = str.size() - 1;
	int equalsCharIndex = 0;

	//Checks that we have only one = in the string
	if (!((equalsCharIndex = str.find('=')) < end && str.find('=', equalsCharIndex + 1) > end))
		return false;

	std::string variableName = str.substr(0, equalsCharIndex);
	std::string variableValue = str.substr(equalsCharIndex + 1, end - equalsCharIndex);

	Helper::rtrim(variableName);
	Helper::trim(variableValue);

	if (!isLegalVarName(variableName))
		throw SyntaxException();

	//Checking if the variableValue is another variable
	Type* type = getVariableValue(variableValue);
	if (type == nullptr) {
		//Checking if the variableValue is value
		type = getType(variableValue);
	}
	else {
		if (getVariableValue(variableName) == nullptr)
			throw NameErrorException(variableName);
		type = type->clone();
	}

	if (type == nullptr)
		throw SyntaxException();

	_variables.erase(variableName);
	_variables.insert(Pair(variableName, type));

	return true;
}

Type * Parser::getVariableValue(const std::string & str)
{
	auto iter =_variables.find(str);
	return iter == _variables.end() ? nullptr : iter->second;
}

bool Parser::runCommand(std::string & str)
{
	std::string variable;
	if (!(variable = Helper::matches(str, "type\\((.*)\\)")).empty()) {
		Type* type = getType(variable);
		if (type != nullptr) {
			std::cout << "<type '" << type->getType() << "'>" << std::endl;
			return true;
		}
		str = variable;
	}

	else if (!(variable = Helper::matches(str, "del (.*)")).empty()) {
		Type* type = getVariableValue(variable);
		if (type != nullptr) {
			_variables.erase(variable);
			return true;
		}
		str = variable;
	}

	else if (!(variable = Helper::matches(str, "len\\((.*)\\)")).empty()) {
		Type* type = getVariableValue(variable);
		if (type == nullptr) {
			type = getType(variable);
		}
		if (type != nullptr) {
			std::cout << type->getLength() << std::endl;
			return true;
		}
		str = variable;
	}

	return false;
}

void Parser::freeVariables()
{
	for (auto iter = _variables.begin(); iter != _variables.end(); iter++) {
		delete iter->second;
	}

	_variables.clear();
}