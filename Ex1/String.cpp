#include "String.h"

String::String(const char * value)
{
	_value = value;
}

String::String(const std::string value)
{
	_value = value;
}

bool String::isPrintable() const
{
	return true;
}

std::string String::toString() const
{
	return _value;
}

Type * String::clone()
{
	String* clone = new String(_value);
	return clone;
}

std::string String::getType()
{
	return "str";
}

int String::getLength()
{
	return _value.size() - 2;
}
