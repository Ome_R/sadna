#ifndef SEQUENCE_H
#define SEQUENCE_H

#include "type.h"

class Sequence : public Type {
	virtual bool isPrintable() const = 0;
	virtual std::string toString() const = 0;
	virtual Type* clone() = 0;
	virtual std::string getType() = 0;
	virtual int getLength() = 0;
};

#endif // SEQUENCE_H