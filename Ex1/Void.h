#ifndef VOID_H
#define VOID_H

#include "type.h"

class Void : public Type {
public:
	Void();
	virtual bool isPrintable() const;
	virtual std::string toString() const;
	virtual Type* clone();
	virtual std::string getType();
	virtual int getLength();
};

#endif // VOID_H