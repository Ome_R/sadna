#ifndef STRING_H
#define STRING_H

#include "Sequence.h"

class String : public Sequence {
public:
	String(const char* value);
	String(const std::string value);
	virtual bool isPrintable() const;
	virtual std::string toString() const;
	virtual Type* clone();
	virtual std::string getType();
	virtual int getLength();
private:
	std::string _value;
};

#endif // STRING_H