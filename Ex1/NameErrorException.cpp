#include "NameErrorException.h"

#include <iostream>

NameErrorException::NameErrorException(const std::string name)
{
	errorMessage = "NameError : name '" + name + "' is not defined";
}

const char * NameErrorException::what() const throw()
{
	return errorMessage.c_str();
}
