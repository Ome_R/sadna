#ifndef TYPE_H
#define TYPE_H

#include <string>

class Type {
private:
	bool _isTemp = false;
public:
	bool isTemp() const;
	void setTemp(const bool isTemp);
	virtual bool isPrintable() const = 0;
	virtual std::string toString() const = 0;
	virtual Type* clone() = 0;
	virtual std::string getType() = 0;
	virtual int getLength() = 0;

};





#endif //TYPE_H
