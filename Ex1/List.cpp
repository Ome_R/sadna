#include "List.h"

List::List(std::vector<Type*> value)
{
	_value = value;
}

List::~List()
{
	for (Type* type : _value) {
		delete type;
	}
}

bool List::isPrintable() const
{
	return true;
}

std::string List::toString() const
{
	std::string string = "[";
	for (int i = 0; i < _value.size(); i++) {
		if (i != 0)
			string += ", ";
		string += _value[i]->toString();
	}

	return string + "]";
}

Type * List::clone()
{
	return this;
}

std::string List::getType()
{
	return "list";
}

int List::getLength()
{
	return _value.size();
}
