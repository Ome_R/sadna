#ifndef LIST_H
#define LIST_H

#include "Sequence.h"
#include <vector>

class List : public Sequence {
public:
	List(std::vector<Type*> value);
	~List();
	virtual bool isPrintable() const;
	virtual std::string toString() const;
	virtual Type* clone();
	virtual std::string getType();
	virtual int getLength();
private:
	std::vector<Type*> _value;
};

#endif // LIST_H