#ifndef BOOLEAN_H
#define BOOLEAN_H

#include "type.h"

class Boolean : public Type {
public:
	Boolean(const bool value);
	virtual bool isPrintable() const;
	virtual std::string toString() const;
	virtual Type* clone();
	virtual std::string getType();
	virtual int getLength();
private:
	bool _value;
};

#endif // BOOLEAN_H