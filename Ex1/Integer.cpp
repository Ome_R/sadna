#include "Integer.h"

#include "TypeError.h"

Integer::Integer(const int value)
{
	_value = value;
}

bool Integer::isPrintable() const
{
	return true;
}

std::string Integer::toString() const
{
	return std::to_string(_value);
}

Type * Integer::clone()
{
	Integer* clone = new Integer(_value);
	return clone;
}

std::string Integer::getType()
{
	return "int";
}

int Integer::getLength()
{
	throw TypeError(getType());
}
