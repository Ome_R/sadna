#pragma once

#include "InterperterException.h"
#include <string>

class TypeError : public InterperterException {
public:
	TypeError(const std::string type);
	virtual const char* what() const throw();
private:
	std::string errorMessage;
};