#ifndef INTEGER_H
#define INTEGER_H

#include "type.h"

class Integer : public Type {
public:
	Integer(const int value);
	virtual bool isPrintable() const;
	virtual std::string toString() const;
	virtual Type* clone();
	virtual std::string getType();
	virtual int getLength();
private:
	int _value;
};

#endif // INTEGER_H