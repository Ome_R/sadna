#ifndef PARSER_H
#define PARSER_H

#include "InterperterException.h"
#include "type.h"
#include "Helper.h"
#include <string>
#include <unordered_map>
#include <iostream>
#include <sstream>

typedef std::unordered_map<std::string, Type*> UnorderedMap;
typedef std::pair<std::string, Type*> Pair;

class Parser
{
public:
	static Type* parseString(std::string str) throw();
	static Type* getType(std::string &str);
	static void freeVariables();

private:

	static bool isLegalVarName(const std::string& str);
	static bool makeAssignment(const std::string& str);
	static Type* getVariableValue(const std::string &str);
	static bool runCommand(std::string& str);

	static UnorderedMap _variables;

};

#endif //PARSER_H
