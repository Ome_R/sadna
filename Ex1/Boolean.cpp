#include "Boolean.h"

#include "TypeError.h"

Boolean::Boolean(const bool value)
{
	_value = value;
}

bool Boolean::isPrintable() const
{
	return true;
}

std::string Boolean::toString() const
{
	return _value ? "True" : "False";
}

Type * Boolean::clone()
{
	Boolean* clone = new Boolean(_value);
	return clone;
}

std::string Boolean::getType()
{
	return "bool";
}

int Boolean::getLength()
{
	throw TypeError(getType());
}
