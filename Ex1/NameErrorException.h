#ifndef NAME_ERROR_EXCEPTION_H
#define NAME_ERROR_EXCEPTION_H

#include "InterperterException.h"
#include <string>

class NameErrorException : public InterperterException {
public:
	NameErrorException(const std::string name);
	virtual const char* what() const throw();
private:
	std::string errorMessage;
};

#endif // NAME_ERROR_EXCEPTION_H