#include "Void.h"

#include "TypeError.h"

Void::Void()
{
}

bool Void::isPrintable() const
{
	return false;
}

std::string Void::toString() const
{
	return std::string();
}

Type * Void::clone()
{
	return new Void();
}

std::string Void::getType()
{
	return "void";
}

int Void::getLength()
{
	throw TypeError(getType());
}
