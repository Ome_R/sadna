#include "TypeError.h"

TypeError::TypeError(const std::string type)
{
	errorMessage = "TypeError: object of type '" + type + "' has no len() .";
}

const char * TypeError::what() const throw()
{
	return errorMessage.c_str();
}
